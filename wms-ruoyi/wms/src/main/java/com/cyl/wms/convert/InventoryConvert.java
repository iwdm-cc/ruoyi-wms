package com.cyl.wms.convert;

import com.cyl.wms.domain.Inventory;
import com.cyl.wms.domain.InventoryHistory;
import com.cyl.wms.pojo.dto.InventoryDTO;
import com.cyl.wms.pojo.vo.InventoryVO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 库存  DO <=> DTO <=> VO / BO / Query
 *
 * @author zcc
 */

/*
@mapper(componentModel = “spring”)起了作用。

该标签可以动态的完成DTO-DO之间的转换，这样程序员就可以免去写DO,VO,DTO各类之间转换关系的代码了。
编译之后，会在target包中生成对应的实现类：
 https://blog.csdn.net/weixin_42891450/article/details/117123216
 */
@Mapper(componentModel = "spring")
public interface InventoryConvert {

    /**
     * @param source DO
     * @return DTO
     */
    InventoryDTO do2dto(Inventory source);

    /**
     * @param source DTO
     * @return DO
     */
    Inventory dto2do(InventoryDTO source);

    List<InventoryVO> dos2vos(List<Inventory> list);

    Inventory inventoryHistory2invertory(InventoryHistory it);
}
