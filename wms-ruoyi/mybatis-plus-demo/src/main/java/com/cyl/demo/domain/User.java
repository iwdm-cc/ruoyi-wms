package com.cyl.demo.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("cyl_user")
@Data
public class User {
    private Long id;
    private String name;
    private Integer age;
    private String email;
}
