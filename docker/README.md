### 1. Docker编排

### 2. 工作目录结构
- 其中db目录存放ruoyi数据库脚本
- 其中jar目录存放打包好的jar应用文件
- 其中conf目录存放redis.conf和nginx.conf配置
- 其中html\dist目录存放打包好的静态页面文件
- 数据库mysql地址需要修改成ruoyi-mysql
- 缓存redis地址需要修改成ruoyi-redis
- 数据库脚本头部需要添加SET NAMES 'utf8';（防止乱码）
