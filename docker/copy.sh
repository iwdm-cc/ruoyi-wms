#!/bin/sh
echo "******欢迎使用部署程序******"
# 复制项目的文件到对应docker路径，便于一键生成镜像。
usage() {
	echo "Usage: sh copy.sh"
	exit 1
}

# copy sql
echo "begin copy sql "
cp ../wms-ruoyi/sql/wms-ruoyi.sql ./mysql/db

# copy html
echo "begin copy html "

# 先删除，后创建，防止包错 dist is not a directory
rm -fr ./nginx/html/dist && mkdir -p ./nginx/html/dist

cp -r ../ruo-yi-wms-vue/dist/** ./nginx/html/dist

# copy jar
echo "begin copy jar"
cp ../wms-ruoyi/ruoyi-admin/target/ruoyi-admin.jar ./wms/jar


