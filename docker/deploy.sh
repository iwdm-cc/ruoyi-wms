#!/bin/sh
echo "******欢迎使用部署程序******"
usage() {
	echo "Usage: sh 执行脚本.sh [start|stop|rm]"
	exit 1
}

# 启动程序模块（必须）
start(){
  # 打包程序
  /bin/sh build.sh
  # 拷贝必要的项目文件
  /bin/sh copy.sh
	docker-compose up -d
}
# 关闭所有环境/模块
stop(){
	docker-compose stop
}
# 删除所有环境/模块
rm(){
	docker-compose rm
}
case "$1" in
start)
  start
  ;;
rm)
  rm
  ;;
stop)
  stop
  ;;
*)
  usage
  ;;

esac
